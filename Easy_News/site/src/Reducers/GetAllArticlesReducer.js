import * as actionTypes from '../Actions/ActionsTypes'

const TOTAL_PER_PAGE = 5;

const initialState = {
    listArticles: [
        {
            id: 1,
            title: "Article 1",
            description: "Description de l'article 1",
            fullarticle: "Ceci est le texte complet de l'article 1.",
        },
        {
            id: 2,
            title: "Article 2",
            description: "Description de l'article 2",
            fullarticle: "Ceci est le texte complet de l'article 2.",
        },
        {
            id: 3,
            title: "Article 3",
            description: "Description de l'article 3",
            fullarticle: "Ceci est le texte complet de l'article 3.",
        },
        {
            id: 4,
            title: "Article 4",
            description: "Description de l'article 4",
            fullarticle: "Ceci est le texte complet de l'article 4.",
        },
        {
            id: 5,
            title: "Article 5",
            description: "Description de l'article 5",
            fullarticle: "Ceci est le texte complet de l'article 5.",
        },
        {
            id: 6,
            title: "Article 6",
            description: "Description de l'article 6",
            fullarticle: "Ceci est le texte complet de l'article 6.",
        },
        {
            id: 7,
            title: "Article 7",
            description: "Description de l'article 7",
            fullarticle: "Ceci est le texte complet de l'article 7.",
        },
        {
            id: 8,
            title: "Article 8",
            description: "Description de l'article 8",
            fullarticle: "Ceci est le texte complet de l'article 8.",
        },
        {
            id: 9,
            title: "Article 9",
            description: "Description de l'article 9",
            fullarticle: "Ceci est le texte complet de l'article 9.",
        }],
    page: 0,
    totalPages: 2,
    TOTAL_PER_PAGE: 5,
    searchText: '',
    searchTotalPages: 1,
    showSearchArticles: 0,
    searchListArticles: [],
};

const GetAllArticlesReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_ARTICLES:
            const totalPagesTmp = Math.ceil(action.payload.length / TOTAL_PER_PAGE);
            const totalPages = totalPagesTmp ? totalPagesTmp : 1;
            return {
                ...state,
                listArticles: action.payload,
                page: 0,
                totalPages: totalPages
            };
        case actionTypes.PAGINATION_SET:
            return {
                ...state,
                page: action.payload
            };
        case actionTypes.PAGINATION_UP:
            return {
                ...state,
                page: state.page + 1
            };
        case actionTypes.PAGINATION_DOWN:
            return {
                ...state,
                page: state.page - 1
            };
        case actionTypes.PAGINATION_RESET:
            return {
                ...state,
                page: 0
            };
        case  actionTypes.SHOW_SEARCH_ARTICLES:
            const searchTextLowerCase = state.searchText;
            const searchListArticles = state.listArticles.filter((item) => {
                const itemLowerCase = item.title.toLowerCase();
                return itemLowerCase.includes(searchTextLowerCase);
            });
            const searchTotalPagesTmp = Math.ceil(searchListArticles.length / TOTAL_PER_PAGE);
            const searchTotalPages = searchTotalPagesTmp ? searchTotalPagesTmp : 1;
            return {
                ...state,
                searchText: action.payload,
                searchListArticles: searchListArticles,
                searchTotalPages: searchTotalPages,
                page: 0,
            };
        case actionTypes.SEARCH_TEXT_INPUT:
            return {
                ...state,
                searchText: action.payload
            };
        case actionTypes.SEARCH_TEXT_CLEAR:
            return {
                ...state,
                searchText: '',
                showSearchArticles: 0,
            };
        case actionTypes.SHOW_SEARCH_ARTICLE_YES:
            return {
                ...state,
                showSearchArticles: 1,
            };
        case actionTypes.SHOW_SEARCH_ARTICLE_NO:
            return {
                ...state,
                showSearchArticles: 0,
            };
        default:
            return state;
    }
};

export default GetAllArticlesReducer;