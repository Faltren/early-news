import React from 'react';
import Carousel from 'react-bootstrap/Carousel';

// CSS
import '../Assets/scss/Home.scss';

// img
import background from '../../public/BackgroundAccueil.png';
import clement_image from '../../public/clement.dedenis.jpg';
import thomas_image from '../../public/thomas.curti.jpg';
import jeremie_image from '../../public/piro_j.jpg';
import vincent_image from '../../public/vincent1.masson.jpg';

import ImageText from "../Components/ImageText";
import HomeBlockLeft from "../Components/HomeBlockLeft";
import {Link} from "react-router-dom";

const Home = () => {
    return (
        <div className="Home">
            <div className="Home_Title">Mieux connaître l'actualité sur les crises mondiales</div>

            <Carousel className="carousel">
                <Carousel.Item>
                    <div>
                        <div className="carouselContainer">
                            <h2>Restez à jour</h2>
                            <h5>Tenez vous à jour sur les crises</h5>
                            <Link to="/list">
                                <p className="carouselContainerSubtitle">En savoir plus ></p>
                            </Link>
                        </div>
                        <img
                            className="d-block w-100 carouselImage"
                            src={background}
                            alt="First slide"
                        />
                    </div>
                    <Carousel.Caption>
                        <p className="carouselCaption">Easy News possède un très grand nombre d'article mis à votre disposition. Toutes ces
                            actualités vous permettront de vous tenir à jour sur les grandes crises actuelles. Easy News
                            s'engage à vous fournir un maximum d'article venant de sources sures.</p>
                    </Carousel.Caption>
                </Carousel.Item>

                <Carousel.Item>

                    <div>
                        <div className="carouselContainer">
                            <h2>Apprenez en jouant</h2>
                            <h5>Apprenez à sauver le monde en jouant</h5>
                            <Link to="/game">
                                <p className="carouselContainerSubtitle">En savoir plus ></p>
                            </Link>
                        </div>
                        <img
                            className="d-block w-100 carouselImage"
                            src={background}
                            alt="First slide"
                        />
                    </div>
                    <Carousel.Caption>
                        <p className="carouselCaption">Easy News est un site mais aussi un jeu ! En jouant avec nous, vous pourrez apprendre en vous
                            amusant les gestes et les Actions permettant de sauver le monde ou gérer les grandes crises.
                            Ce jeu est mis à jour régulièrement et utilise les mêmes sources d'informations que pour nos
                            articles.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>

            <h3 className="subTitle">Aidez vos enfants</h3>

            <HomeBlockLeft
                imageText={"Jouez et apprenez"}
                title={"Aidez vos enfants à apprendre"}
                text={"Le jeu Easy News est jouable par les grands mais aussi par les petits ! En jouant à notre jeu, vous permettrez à vos jeunes proches de se renseigner sur les grands problèmes et ainsi de mieux se protéger faces à des pandémies ou face aux désastres naturelles."}
                link={"/game"}
            />

            <h3 className="subTitle">Qui sommes nous ?</h3>

            <div>
                <div className="imagesFooter">
                    <ImageText text={"Clément Dedenis"} image={clement_image} GithubText={"@Clementdds"} GithubLink={"https://github.com/Clementdds"}/>
                    <ImageText text={"Jérémie Piro"} image={jeremie_image} GithubText={"@JeremiePiro"} GithubLink={"https://github.com/JeremiePiro"}/>
                    <ImageText text={"Thomas Curti"} image={thomas_image} GithubText={"@ThomasCurti"} GithubLink={"https://github.com/ThomasCurti"}/>
                    <ImageText text={"Vincent Masson"} image={vincent_image} GithubText={"@VincentEpita"} GithubLink={"https://github.com/VincentEpita"}/>
                </div>
            </div>

        </div>
    );
};

export default Home;
