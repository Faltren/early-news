import React from "react";

const ImageText = ({image = "", text = "", GithubLink = "", GithubText = ""}) => {
    return (
      <div>
          <img className="imagesFooterImage" alt={image} src={image}/>
          <div className="imagesFooterText">{text}</div>
          <a href={GithubLink} className={"imageFooterLink"} target="_blank" rel="noopener noreferrer">
              <div className="imagesFooterText">{GithubText}</div>
          </a>
      </div>
    );
};

export default ImageText;