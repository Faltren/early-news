// Require
const axios = require('axios');
const https = require('https');

// For Development api
const agent = new https.Agent({
    rejectUnauthorized: false,
});

// Create client
export const client = axios.create({
    responseType: 'json',
    withCredentials: false,
    httpsAgent: agent
});

export const getAllArticlesApiCall = process.env.API_ARTICLE_ENDPOINT;