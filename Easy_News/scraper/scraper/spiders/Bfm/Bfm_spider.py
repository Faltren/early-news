import scrapy
from scraper.items import Article
from scrapy.loader import ItemLoader


def parse_article(response):
    def extract_with_css(query):
        return response.css(query).get(default='').strip()

    def extract_all_with_css(query):
        return response.css(query).getall()

    article = ItemLoader(item=Article())
    article.add_value('title', extract_with_css('h1#contain_title::text'))
    article.add_value('description', extract_with_css('div.chapo::text'))
    full_article = extract_all_with_css("div.content_body p")
    for paragraph in full_article:
        article.add_value('full_article', paragraph)
    article.add_value('source', response.url)
    return article.load_item()


class BfmSpider(scrapy.Spider):
    name = "Bfm"
    start_urls = [
        'https://www.bfmtv.com/sante/actualites/?page=1',
    ]

    def parse(self, response):
        for news in response.css('article.content_type_article'):
            article = news.css('article a::attr(href)').get()
            if article is not None:
                yield response.follow(article, parse_article)

        for pagination_links in response.css('ul.pagination'):
            nextPage = pagination_links.css('li a::attr(href)').get()
            if nextPage is not None:
                yield response.follow(nextPage, callback=self.parse)
