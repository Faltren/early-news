# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import json

from scrapy.exporters import JsonLinesItemExporter

from scraper.datascience.kwextract.kwextracter\
    import KwExtracter
from scrapy.loader import ItemLoader
from scrapy.utils.serialize import ScrapyJSONEncoder
from scraper.items import DubiousArticle
import requests


class JsonWriterPipeline(object):

    def __init__(self):
        self.headers = {'Content-type': 'application/json', 'Accept': 'application/json'}

    def open_spider(self, spider):
        # Get Source from backend
        apiGetArticleSource = spider.settings.get('API_ARTICLE_SOURCE')
        response = requests.get(url=apiGetArticleSource, headers=self.headers, verify=False)
        self.articleSource = response.json()

    def process_item(self, item, spider):
        # Get source Id from spider
        source_id = [source['id'] for source in self.articleSource if source['name'] == spider.name]

        # Initialise KeyWordExtracter
        extracter = KwExtracter()

        if item.get('full_article', '') == '':
            return item

        # Get object ready
        dubiousArticle = ItemLoader(item=DubiousArticle())
        dubiousArticle.add_value('title', item.get('title', 'title'))
        dubiousArticle.add_value('sourceId', source_id[0])
        dubiousArticle.add_value('fullArticleSource', item.get('description', ''))
        dubiousArticle.add_value('fullArticleSource', '###')
        dubiousArticle.add_value('fullArticleSource', item.get('full_article', ''))
        # Get keyword extract
        dubiousArticle.add_value('kwFrequency', extracter.kwextract(item.get('full_article', 'full_article')).__str__())

        post = dubiousArticle.load_item()

        jsonItem = json.dumps(dict(post), cls=ScrapyJSONEncoder)

        # Get Api url
        apiSetDubiousArticle = spider.settings.get('API_DUBIOUS_ARTICLE')
        # call to Backend API, api/DubiousArticle
        r = requests.post(url=apiSetDubiousArticle, data=jsonItem, headers=self.headers, verify=False)
        return item
