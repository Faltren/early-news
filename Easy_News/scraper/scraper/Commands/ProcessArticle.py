from scrapy.commands import ScrapyCommand
from scrapy.utils.project import get_project_settings
from ..datascience.ProcessDubiousArticle.ProcessDubiousArticle import ProcessDubiousArticle


class ProcessArticleCommand(ScrapyCommand):
    requires_project = True

    def syntax(self):
        return '[options]'

    def short_desc(self):
        return 'Process dubious articles into articles'

    def run(self, args, opts):
        settings = get_project_settings()
        processArticle = ProcessDubiousArticle(settings)
        processArticle.process()
