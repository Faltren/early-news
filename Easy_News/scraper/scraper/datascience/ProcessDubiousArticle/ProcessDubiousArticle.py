import json
import requests
import pandas as pd
from ast import literal_eval
from scrapy.utils.serialize import ScrapyJSONEncoder

from ..articlecompare.articlecomparer import ArticleComparer


def responseToList(response):
    items = []
    for item in response.json():
        text = item['fullArticleSource'].split('###', 1)
        element = {'id': item['id'],
                   'title': item['title'],
                   'description': text[0],
                   'text': text[1],
                   'kwFrequency': item['kwFrequency'],
                   'sourceId': item['sourceId']}
        items.append(element)
    return items


class ProcessDubiousArticle:
    def __init__(self, settings):
        self.settings = settings
        self.comparer = ArticleComparer()
        self.headers = {'Content-type': 'application/json', 'Accept': 'application/json'}

    def __getListComp(self, data):
        listcomps = []
        for index, row in data.iterrows():
            string1 = row['kwFrequency']
            source1 = row['sourceId']
            l1 = literal_eval(string1)
            for index2, row2 in data.iterrows():
                if index2 > index:
                    source2 = row2['sourceId']
                    string2 = row2['kwFrequency']
                    l2 = literal_eval(string2)
                    dis = self.comparer.areSimilar(l1, l2)
                    if (source1 != source2) and dis:
                        if row['id'] not in listcomps:
                            listcomps.append(row['id'])
                        if row2['id'] not in listcomps:
                            listcomps.append(row2['id'])
        return listcomps

    def removeArticleFromDb(self):
        # Get Api url
        apiSetDubiousArticle = self.settings.attributes.get('API_DUBIOUS_ARTICLE').value
        # call to Backend API, api/DubiousArticle
        r = requests.delete(url=apiSetDubiousArticle, verify=False)
        return r

    def process(self):
        # Get dubious articles from backend
        response = requests.get(url=self.settings.attributes.get('API_DUBIOUS_ARTICLE').value, headers=self.headers,
                                verify=False)

        # items in the response
        items = responseToList(response)
        data = pd.DataFrame.from_records(items)

        # list of comparisons
        listcomps = self.__getListComp(data)
        for item in items:
            if item['id'] in listcomps:
                element = {'title': item['title'],
                           'description': item['description'],
                           'fullarticle': item['text'],
                           'sourceId': item['sourceId']}
                print(element)
                # Get Api url
                apiSetArticle = self.settings.attributes.get('API_REAL_ARTICLE').value
                jsonItem = json.dumps(element, cls=ScrapyJSONEncoder)

                # call to Backend API, api/Article
                r = requests.post(url=apiSetArticle, data=jsonItem, headers=self.headers, verify=False)
