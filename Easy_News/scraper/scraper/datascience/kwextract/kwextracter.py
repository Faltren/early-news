import re
import os.path
import Stemmer

from os import path

CHARACTERS_FILENAME = os.path.join(os.path.dirname(__file__), 'characters.txt')
SKIPWORDS_FILENAME = os.path.join(os.path.dirname(__file__), 'skipwords.txt')


class KwExtracter:
    def __init__(self):
        self.stemmer = Stemmer.Stemmer('french')

        if not path.exists(CHARACTERS_FILENAME):
            raise FileNotFoundError("KwExtracter: Instanciation: characters.txt was not found")
        if not path.exists(SKIPWORDS_FILENAME):
            raise FileNotFoundError("KwExtracter: Instanciation: skipwords.txt was not found")

        content = ''
        with open(CHARACTERS_FILENAME, encoding='utf-8') as f:
            self.characters = [line.rstrip('\n') for line in f]

        self.skipwords = []
        with open(SKIPWORDS_FILENAME, encoding='utf-8') as f:
            content = [line.rstrip('\n') for line in f]
        for line in content:
            if line == '' or line[0] == ' ':
                continue
            line = line.split(' ')[0]
            self.skipwords.append(line)

        self.words = []
        self.freq = {}
        self.orderedFreq = []

    def __addWord(self, stemmedword, realword):
        if stemmedword == '':
            return
        if realword in self.skipwords:
            return
        self.words.append(stemmedword)

    def __addOneToDict(self, word):
        if word in self.freq:
            self.freq[word] += 1.0
        else:
            self.freq[word] = 1.0

    def kwextract(self, text):
        # Reinitialisation
        self.words = []
        self.freq = {}
        self.orderedFreq = []

        # Construction de la liste de mots
        current = ''
        for c in text.lower():
            if c not in self.characters:
                self.__addWord(self.stemmer.stemWord(current), current)
                current = ''
            else:
                current += c
        self.__addWord(self.stemmer.stemWord(current), current)

        # Calcul des fréquences des mots clés
        for w in self.words:
            self.__addOneToDict(w)
        sorted_keys = sorted(self.freq, key=self.freq.get, reverse=True)
        for i, w in enumerate(sorted_keys):
            if i < 30:
                self.orderedFreq.append((w, round(self.freq[w] / len(self.words), 4)))

        # Valeur retournée adaptée selon la longueur de l'article
        res = [ x for x in self.orderedFreq if x[1] > 0.008]
        if len(res) > 20:
            res = res[:20]
        return res

    def fromFile(self, file):
        if not path.exists(file):
            raise FileNotFoundError("KwExtracter: fromFile: file was not found : " + file)
        with open(file, encoding='utf-8') as f:
            text = f.read().replace('\n', '')
            res = self.kwextract(text)
        return res
