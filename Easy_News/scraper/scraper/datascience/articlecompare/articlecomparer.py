class ArticleComparer:
    def __init__(self):
        self.MINIMUM_SCORE = 150

    def areSimilar(self, kwlist1, kwlist2):
        score = 0
        for i, x in enumerate(kwlist1):
            for j, y in enumerate(kwlist2):
                if x[0] == y[0]:
                    score += (20 - i) + (20 - j)
        return score >= self.MINIMUM_SCORE