# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import Join, MapCompose, TakeFirst
from w3lib.html import remove_tags


def process_float_or_int(value):
    try:
        return eval(value)
    except:
        return value


class Article(scrapy.Item):
    title = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=Join(),
    )
    description = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=Join(),
    )
    full_article = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=Join(),
    )
    source = scrapy.Field()


class DubiousArticle(scrapy.Item):
    title = scrapy.Field(
        output_processor=Join(),
    )
    sourceId = scrapy.Field(
        input_processor=MapCompose(lambda x: process_float_or_int(x)),
        output_processor=TakeFirst()
    )
    fullArticleSource = scrapy.Field(
        input_processor=MapCompose(),
        output_processor=Join(),
    )
    kwFrequency = scrapy.Field(
        output_processor=Join(),
    )
