# Scraper

## Requirement

```
pipenv
```

## To Launch

Launch pipenv shell :  

```
pipenv install --dev
pipenv shell
```  

Now you can launch Scrapy in terminal:

```
scrapy
```

To have the list of all commands:

```
scrapy --help
```

To launch all spiders:

```
scrapy runAllSpiders
```

To launch all spiders in a scheduler:

```
scrapy scheduleSpider
```