﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Backend.DataAccess;
using Backend.Dbo.Model;
using Backend.Logger;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Serilog;

namespace Backend.Controllers
{
    [Route("api/Article")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class ArticleController : ControllerBase
    {
        private readonly ArticleRepository _articleRepository;
        private LogRepository _logRepository;
        private ILogger _logger;
        private bool _log;

        public ArticleController(ArticleRepository articleRepository, ILogger logger, bool log = true)
        {
            _articleRepository = articleRepository;
            _articleRepository.DoLog = log;
            _log = log;
            _logger = logger;
        }

        // GET: api/Article
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _articleRepository.Get());
        }

        // GET: api/Article/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var val = _articleRepository.Get(id).Result;

            try
            {
                List<Dbo.Model.article> list = new List<Dbo.Model.article>(val);
                if (list[0] == null)
                    return NotFound();
                return Ok(list[0]);
            }
            catch (Exception e)
            {
                if (_log)
                    await Logger.Logger.LogError(e, "ArticleController", _logRepository);
                Console.WriteLine(e.ToString());
                return NotFound();
            }

        }

        // POST: api/Article
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] JsonElement value)
        {
            Dbo.Model.article article = null;
            try
            {
                string val = System.Text.Json.JsonSerializer.Serialize(value);
                article = JsonConvert.DeserializeObject<article>(val);
            }
            catch (Exception e)
            {
                _logger.Error("Tried to insert article but the value was wrong");
                _logger.Error(e.Message);
                return NotFound();
            }
            await _articleRepository.Insert(article);
            return Ok();
        }
    }
}
